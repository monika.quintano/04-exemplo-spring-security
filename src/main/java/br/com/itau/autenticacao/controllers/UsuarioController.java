package br.com.itau.autenticacao.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.autenticacao.models.Usuario;
import br.com.itau.autenticacao.services.UsuarioService;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

	@Autowired
	UsuarioService usuarioService;
	
	@GetMapping
	public ResponseEntity consultar(Authentication authentication) {
		int idUsuarioLogado = (int) authentication.getPrincipal();
		
		Optional<Usuario> usuarioOptional = usuarioService.buscarPorId(idUsuarioLogado);
		
		return ResponseEntity.ok(usuarioOptional.get());
	}
	
	@PostMapping
	public ResponseEntity inserir(@RequestBody Usuario usuario) {
		Usuario usuarioInserido = usuarioService.inserir(usuario);
		
		return ResponseEntity.status(201).body(usuarioInserido);
	}
}
